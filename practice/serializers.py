from pprint import pprint 

from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction

from rest_framework import serializers

from practice.models import Album, Track


class TrackSerializer(serializers.ModelSerializer):

    id = serializers.IntegerField(required=False)
    
    class Meta:
        model = Track
        fields = ('id', 'order', 'title', 'duration')


class AlbumSerializer(serializers.ModelSerializer):

    tracks = TrackSerializer(many=True)

    class Meta:
        model = Album
        fields = ('id', 'name', 'artist', 'tracks')

    def create(self, validated_data):
        
        tracks = validated_data.pop("tracks")

        with transaction.atomic():
            album = Album.objects.create(**validated_data)
            
            for track in tracks:
                Track.objects.create(**track, album=album)

        return album

    def update(self, instance, validated_data):

        tracks = validated_data.pop("tracks")

        track_ids = instance.tracks.values_list("id")
        album_tracks = []

        # Need to filter the tracks of the current album
        # otherwise you can end up updating tracks of the other album, not the current one
        # This is helpful in 2 cases - 
        # 1. If someone is trying to mess with the database by manually passing different ids from front end.
        # 2. During development, developer could send different id by mistake.

        for track in tracks: 
            entry = list(track.items())[0]
            if entry[0] == "id":
                tuple_entry = (entry[1],)
                if tuple_entry in track_ids: 
                    album_tracks.append(track)

        keep_tracks = []


        # Updating the current album instance
        instance.name = validated_data["name"]
        instance.artist = validated_data["artist"]
        

        # All the saving, creating and deleting needs to happen inside a transaction.
        # Otherwise database will end up having serious integrity issues.
        # You could be fired for removing transaction from here.
        with transaction.atomic():
            instance.save() # save() needs to be called inside a transaction
  
            #  if you iterate over tracks, you will end up updating tracks that might not belong to current album.
            #  Always iterate over tracks of the current album
            for track in album_tracks:
                if "id" in track.keys():
                    try:
                        t = Track.objects.get(id=track["id"])

                        t.order = track["order"]
                        t.title = track["title"]
                        t.duration = track["duration"]
                        t.save()  # save() needs to be called inside a transaction

                    except ObjectDoesNotExist:
                        pass 
                else: 
                    t = Track.objects.create(**track, album=instance) # create() needs to be called inside a transaction
                    
                # will append for both of the above if and else scenarios
                keep_tracks.append(t.pk)

            for track in instance.tracks:
                if track.id not in keep_tracks:
                    track.delete()  # delete() needs to be called inside a transaction

        return instance

