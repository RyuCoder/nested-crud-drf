from django.contrib import admin


from practice import models


class TrackAdmin(admin.ModelAdmin):
    list_display = ['id', 'album', 'order', 'title', 'duration']

admin.site.register(models.Album)
admin.site.register(models.Track, TrackAdmin)
