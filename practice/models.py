from django.db import models


class Track(models.Model):
    album = models.ForeignKey("practice.Album", on_delete=models.CASCADE)
    order = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    duration = models.CharField(max_length=255)
    
    def __str__(self): 
        return self.title


class Album(models.Model):

    name = models.CharField(max_length=255)
    artist = models.CharField(max_length=255)

    @property
    def tracks(self): 
        return self.track_set.all()
        # return Track.objects.filter(album=self.pk)

    def __str__(self):
        return self.name
