from rest_framework import viewsets

from practice.serializers import AlbumSerializer, TrackSerializer
from practice.models import Album, Track

class AlbumViewset(viewsets.ModelViewSet):
    serializer_class = AlbumSerializer

    def get_queryset(self):
        return Album.objects.all()


class TrackViewset(viewsets.ModelViewSet):
    serializer_class = TrackSerializer

    def get_queryset(self):
        return Track.objects.all()
