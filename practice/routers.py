from rest_framework.routers import SimpleRouter, Route


class CLDUDRouter(SimpleRouter):
    routes = [

        # Create View
        Route(
            url=r'^{prefix}/create{trailing_slash}$',
            mapping={
                'post': 'create'
            },
            name='{basename}-create',
            detail=False,
            initkwargs={'suffix': 'Create'}
        ),

        # List View
        Route(
            url=r'^{prefix}/list{trailing_slash}$',
            mapping={
                'get': 'list',
            },
            name='{basename}-list',
            detail=False,
            initkwargs={'suffix': 'List'}
        ),

        # Detail View
        Route(
            url=r'^{prefix}/detail/{lookup}{trailing_slash}$',
            mapping={
                'get': 'retrieve',
            },
            name='{basename}-detail',
            detail=True,
            initkwargs={'suffix': 'Instance'}
        ),

        # Update View
        Route(
            url=r'^{prefix}/update/{lookup}{trailing_slash}$',
            mapping={
                'put': 'update',
                'patch': 'partial_update',
            },
            name='{basename}-update',
            detail=True,
            initkwargs={'suffix': 'Instance'}
        ),

        # Delete View
        Route(
            url=r'^{prefix}/delete/{lookup}{trailing_slash}$',
            mapping={
                'delete': 'destroy'
            },
            name='{basename}-delete',
            detail=True,
            initkwargs={'suffix': 'Instance'}
        ),
    ]
