"""demo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path


from practice.routers import CLDUDRouter
from practice.views import AlbumViewset, TrackViewset

album_router = CLDUDRouter()
track_router = CLDUDRouter()


album_router.register(r'albums', AlbumViewset, basename='albums')
track_router.register(r'tracks', TrackViewset, basename='tracks')


urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += album_router.urls
urlpatterns += track_router.urls
